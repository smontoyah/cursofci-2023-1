import math
import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico():
    def __init__(self, velinit, alpha, g, h0, x0):
        print("inicializando clase tiroParabolico")

        # Parametros

        self.velinit = velinit
        self.radianalpha = math.radians(alpha)
        self.g = g
        self.h0 = h0
        self.x0 = x0

    def velX(self):
        vel_x = self.velinit * round(math.cos(self.radianalpha), 3)
        return vel_x

    def velY(self):
        vel_y = self.velinit * round(math.sin(self.radianalpha), 3)
        return vel_y

    def tMaxVuelo(self):
        try:
            tmax = (-(self.velY()) - np.sqrt(self.velY() ** 2 - 2 * self.g * self.h0)) / (self.g)
            return tmax
        except:
            return "error en calculo de tmax, revise parametros"

    def arrTime(self):
        arr_time = np.arange(0, self.tMaxVuelo(), 0.001)
        return arr_time

    def posX(self):
        posx = [self.x0 + i * self.velX() for i in self.arrTime()]
        return posx

    def posY(self):
        posy = [self.h0 + i * self.velY() + 0.5 * self.g * i ** 2 for i in self.arrTime()]
        return posy

    def figParabolico(self):
        plt.figure(figsize=(10, 8))
        plt.plot(self.posX(), self.posY())
        plt.savefig("parabolico1.png")



   


    