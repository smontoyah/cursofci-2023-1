import caminata as cm

N = 100 #Número de pasos

if __name__=='__main__':
    #Apliquemos la clase camino
    a=cm.camino(N)
    #Se generan las posiciones
    a.posiciones()
    #Se grafican las posiciones
    a.grafica()
    