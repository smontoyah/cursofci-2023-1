import numpy as np
import pandas as pd
import pandas as pd
import pandas as pd
from scipy import interpolate    # librerias necesarios para el calculo
import numpy as np                         
from matplotlib import animation
import scipy as sp
import scipy.optimize 
from scipy import interpolate 
import pandas as pd
from scipy import interpolate 
from scipy.misc import derivative
import scipy.integrate as integrate
import scipy.optimize as optimize
from scipy.optimize import minimize
from scipy.integrate import quad
import matplotlib.pyplot as plt
# procedemos a realizar el ejercicio. tenemos que realizar la integracion correspondiente:
class monte_carlo():
    def __init__(self,N,b,a,f,n):  # definimos la clase
        
        self.N=N # numero de puntos
        self.b=b    #numero mayor del intervalo
        self.a=a    # numero menor del intervalo
        self.f=f    # funcion
        self.n=n    # numero de puntos de la funcion

    def arreglo(self):   # arreglo de numeros
        y=np.random.uniform(self.a,self.b)  
        return y # retornamos el numero





    def calculo(self):  # definimos la funcion usando el metodo de montecarlo
        E=0
        lista=[]
        for i in range(self.N):
            x=np.random.uniform(self.a,self.b)    
            E=E+ (np.cos(x))*x**2
            lista.append(E)
            

        E=E/self.N     
        return(E*(self.b-self.a))   
    def analitico_scipy(self):   # metodo scipy analitico de la libreria
        result = integrate.quad(lambda x: np.cos(x)*x**2, 0, np.pi)
        return result[0]
    def calculo1(self):   # funcion calculo de todas los valores
        E=0
        lista=[]
        for i in range(self.N):
            x=np.random.uniform(self.a,self.b)    
            E=E+ (np.cos(x))*x**2
            lista.append(E)
        return lista  
    def funcion_numero(self):  # numero de puntos a considerar
         arreglo = np.arange(1, (self.n)+1)
         return arreglo

    def funcion_real(self):  # funcion real para tener las soluciones.
        x1 = np.random.uniform(self.a, self.b, self.n)
        solucion_funcion = (self.b-self.a) * np.cumsum(self.f(x1)) / self.funcion_numero() 
        return solucion_funcion
    def grafica(self): # grafica.
        plt.figure(figsize=(10,8))
        plt.plot(self.funcion_numero(), self.funcion_real(),"r",label="montecarlo")
        plt.axhline(y=self.analitico_scipy(), xmin=0, xmax=np.pi,label="scipy")
        plt.legend()
        plt.xlabel("numero de puntos")
        plt.ylabel("solucion de la integral")
        plt.grid()
        plt.savefig("montecarlo1.png")
        plt.show()
        
 
        
    



        

        

