import numpy as np
from scipy import integrate

"""
clase que calcula la integral de una función entre a y b
"""
class Integral:
    def __init__(self,f,a,b):
        self.f=f        # función a integrar
        self.a=a        # límite inferior
        self.b=b        # límite superior

    def Analitica(self):
        I_analitica=integrate.quad(self.f,self.a,self.b)[0]
        #print(I_analitica)
        return I_analitica
    
"""
clase que calcula la integral de una función entre a y b
usando el método suma de áreas con Montecarlo
"""
class MonteCarlo(Integral):
        def __init__(self,f,a,b,N):
            super().__init__(f,a,b)
            self.N=N        # número de puntos

        def Calculo(self):
            x=np.random.uniform(self.a,self.b,self.N)          # arreglo N números aleatorios entre a y b
            I_mc=0
            for i in x:
                I_mc+=(self.b-self.a)/self.N*self.f(i)
            return I_mc     
