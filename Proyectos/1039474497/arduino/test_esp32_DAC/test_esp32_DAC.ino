void setup() {
  Serial.begin(115200);
}

void loop() {
 if(Serial.available()>0){
  char caracter=Serial.read();
  if(caracter=='U'){
    float upper_voltage=Serial.parseFloat();
    int upper_dac=int(upper_voltage*255.0/3.3);
    dacWrite(25, upper_dac);
    Serial.print('U'); Serial.println(upper_voltage);
    }else if(caracter=='L'){
    float lower_voltage=Serial.parseFloat();
    int lower_dac=int(lower_voltage*255.0/3.3);
    dacWrite(26, lower_dac);
    Serial.print('L'); Serial.println(lower_voltage);
  }
 }
}
