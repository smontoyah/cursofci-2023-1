const byte signalPin = 2; // Pin de entrada de la señal
volatile unsigned int pulseCount = 0; // Contador de pulsos
unsigned long previousMillis = 0; // Tiempo en milisegundos del último cambio de umbral
unsigned long tw = 0; // Tiempo en milisegundos entre cambios de umbral
unsigned long t_max = 0; // Tiempo máximo de ejecución en milisegundos
const int th_step = 100; // mv
unsigned int th_low = 0; // Umbral inferior
unsigned int th_high = th_low + th_step; // Umbral superior
String datos[30];

void setup() {
  Serial.begin(9600); // Iniciar comunicación serial
  pinMode(signalPin, INPUT_PULLUP); // Configurar pin de entrada de la señal como pull-up
  attachInterrupt(digitalPinToInterrupt(signalPin), pulse, FALLING); // Configurar interrupción en el pin de entrada de la señal
}

void loop() {
  if (Serial.available() > 0) { // Si hay datos en el puerto serial
    String command = Serial.readStringUntil('\n'); // Leer el comando
    if (command.startsWith("tw")) { // Si el comando es para configurar tw
      tw = command.substring(2).toInt(); // Obtener el valor de tw
    } else if (command.startsWith("ts")) { // Si el comando es para configurar t_max
      t_max = command.substring(2).toInt(); // Obtener el valor de t_max
    } else if (command.equals("start")) { // Si el comando es para iniciar la ejecución
      unsigned long startTime = millis(); // Obtener el tiempo de inicio
      while (millis() - startTime < t_max) { // Ejecutar durante t_max milisegundos
        if (millis() - previousMillis >= tw) { // Si ha pasado el tiempo de cambio de umbral
          th_low += th_step; // Incrementar umbral inferior
          th_high += th_step; // Incrementar umbral superior
          previousMillis = millis(); // Actualizar el tiempo del último cambio de umbral
          unsigned int count = pulseCount; // Obtener el contador de pulsos
          pulseCount = 0; // Reiniciar el contador de pulsos

          // Enviar datos por serial cuando th_high llega a 3000
          if (th_high == 3000) {
            //Serial.println("Enviando datos por serial...");
            for (int i = 0; i < 30; i++) {
              Serial.print(datos[i]);
              Serial.print(",");
            }
            Serial.println();
            th_low = 0; // Reiniciar umbral inferior
            th_high = th_step; // Reiniciar umbral superior
          }

          // Agregar datos al arreglo
          datos[(th_high / th_step) - 1] = String((th_low + th_high) / 2) + "*" + String(count);
        }
        if (millis() - startTime >= t_max) { // Si se llegó a t_max
          Serial.println("finished"); // Enviar "finished" por serial
          break; // Salir del bucle while
        }
      }
    }
  }
}

void pulse(){
  pulseCount++;
}
