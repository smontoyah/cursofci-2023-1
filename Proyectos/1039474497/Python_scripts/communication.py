
#****************** Libraries ***********************************
import serial, time, json, re

#****************** Functions ***********************************

hw_sensor = serial.Serial(port='COM7', baudrate=115200, timeout=1, write_timeout=1)

def remove_newline(list_of_strings):
        return [string.rstrip('\r\n') for string in list_of_strings]

def separar_elementos(lista):
    lista_x = []
    lista_y = []
    lista_z = []

    for item in lista:
        x, y_z = re.split(r'%', item)
        y, z = map(float, re.split(r'\$', y_z))
        z = int(z)
        lista_x.append(float(x))
        lista_y.append(y)
        lista_z.append(z)

    return lista_x, lista_y, lista_z


#****************** Variables ***********************************

t_end = time.time() + 1250 # tiempo por el que se van a tomar datos

data = []
ThL_list = [] 
ThH_list = [] 
counter_list = []


#***************************************************************

if __name__ == '__main__':
    while time.time() < t_end:
        hw_sensor.write('getValue'.encode('utf-8'))
        time.sleep(0.1)
        try:
            raw_string_b = hw_sensor.readline()
            raw_string_s = raw_string_b.decode('utf-8')
            
            #print(raw_string_s)
            data.append(raw_string_s)
            data1 = remove_newline(data) #hasta aqui, la lista contiene los datos sin \r\n
            ThL_list, ThH_list, counter_list = separar_elementos(data1) # separo por listas los ThL, ThH y counter

        except:
             print("Exception occurred, somthing wrong...")
    hw_sensor.close()

    #verificamos que este la misma cantidad de leementos en las listas
    print(len(data1))
    print(len(ThL_list))
    print(len(ThH_list))
    print(len(counter_list))

#******************************* Plotting *****************************
    import matplotlib.pyplot as plt

    def plot_lists(list1, list2):
        plt.plot(list1, list2, 'o')
        plt.xlabel('Threshold level')
        plt.ylabel('Counts')
        plt.title('Plot of List Elements in Pairs')
        plt.show()


    plot_lists(ThH_list, counter_list)