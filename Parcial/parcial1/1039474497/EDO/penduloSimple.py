import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

class PenduloSimple:
    def __init__(self, L, g, theta0, omega0, dt, tmax):
        self.L = L # longitud del hilo del péndulo
        self.g = g # aceleración debida a la gravedad
        self.theta0 = theta0 # ángulo inicial
        self.omega0 = omega0 # velocidad angular inicial
        self.dt = dt # paso de tiempo
        self.tmax = tmax # tiempo máximo de la simulación
        
    def euler(self):
        # Inicialización de las variables
        theta = np.zeros(int(self.tmax/self.dt)+1)
        omega = np.zeros(int(self.tmax/self.dt)+1)
        t = np.arange(0, self.tmax+self.dt, self.dt)
        theta[0] = self.theta0
        omega[0] = self.omega0
        
        # Resolución de la ecuación diferencial utilizando el método de Euler
        for i in range(len(t)-1):
            omega[i+1] = omega[i] - (self.g/self.L)*np.sin(theta[i])*self.dt
            theta[i+1] = theta[i] + omega[i+1]*self.dt
        
        # Graficación de los resultados
        fig, ax = plt.subplots()
        ax.set_title("$\Theta$(t) y $\omega$(t) metodo Euler")
        ax.plot(t, theta, label=r'$\theta(t)$')
        ax.plot(t, omega, label=r'$\omega(t)$')
        ax.set_xlabel('Tiempo (s)')
        ax.set_ylabel('Ángulo (rad) / Velocidad angular (rad/s)')
        fig.savefig("grafica_met_euler.png")
        ax.legend()
        
        
    def desplazamientoAngular(self):
        # Definición de la función que describe la ecuación diferencial
        def pendulo(y, t, L, g):
            theta, omega = y
            dydt = [omega, -(g/L)*np.sin(theta)]
            return dydt
        
        # Condiciones iniciales y tiempo de simulación
        y0 = [self.theta0, self.omega0]
        t = np.arange(0, self.tmax+self.dt, self.dt)
        
        # Resolución de la ecuación diferencial utilizando odeint
        sol = odeint(pendulo, y0, t, args=(self.L, self.g))
        theta = sol[:,0]
        omega = sol[:,1]
        
        # Graficación de los resultados
        fig, ax = plt.subplots()
        ax.plot(t, theta, label=r'$\theta(t)$')
        ax.plot(t, omega, label=r'$\omega(t)$')
        ax.set_title("$\Theta$(t) y $\omega$(t) desplazamiento angular")
        ax.set_xlabel('Tiempo (s)')
        ax.set_ylabel('Ángulo (rad) / Velocidad angular (rad/s)')
        fig.savefig("grafica_desplazamiento_ang.png")
        ax.legend()
       
